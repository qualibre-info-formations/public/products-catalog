// components/Favorites.js

import React from 'react'
import {Image, View} from 'react-native'
import {Route} from "@react-navigation/native";

export interface Picture {
    height: number,
    width: number,
    uri: string
}

const PictureView = (props: {route: Route<'Image', {picture: Picture}> }) => {
    const {picture} = props.route.params;
    return (<View style={{flex: 1}}>
        <Image style={{height: picture.height, width: picture.width}} source={{uri: picture.uri}} />
    </View>)
}

export default PictureView
