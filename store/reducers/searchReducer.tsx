// Store/Reducers/productsReducer.js

import {ProductsListProps} from "../../components/ProductsList";
import {AnyAction} from "redux";

const initialState = { searchText: '' };

export default function storeSearchValue(state = initialState, action: AnyAction) {
    switch (action.type) {
        case 'STORE_SEARCH':
            return {...state, searchText: action.searchText};
        default:
            return state;
    }
}