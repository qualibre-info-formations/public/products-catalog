import {Dispatch} from "redux";

const API_KEY = "5d796207b5eedc635380d9b3"; // Replace with your API key for restdb.io

export const fetchProducts = () => {
    return (dispatch: Dispatch) => {
        fetch('https://productcatalog-4b5d.restdb.io/rest/products', {
            headers: {'x-apikey': API_KEY}
        }).then((result) => {
            if (result.ok) {
                return result.json()
            }
        }).then((data) => {
            dispatch({type: 'STORE_PRODUCTS', products: data});
        }).catch((err) => {
            console.error(err);
        });
    }
}