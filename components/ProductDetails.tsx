// components/ProductDetails.js

import React from 'react';
import {StyleSheet, View, Text, Image, Share, TouchableOpacity, Platform, ActivityIndicator} from 'react-native';
import {Product} from "./ProductItem";
import {connect} from 'react-redux';
import {NavigationProp, RouteProp} from "@react-navigation/core";

interface ProductItemState {
    product: Product
}

interface ProductDetailsProps {
    products: Array<Product>
    route: RouteProp<any, any>
    navigation: NavigationProp<any>
}

class ProductDetails extends React.Component<ProductDetailsProps, ProductItemState> {

    state: ProductItemState = {
        product: {
            id: -1,
            name: "",
            description: "string",
            stock: 0,
            price: 0,
            picture: ""
        }
    };

    shareProduct = () => {
        const {name, price, picture} = this.state.product;
        Share.share({title: name, message: `${name}, ${price}€, ${picture}`, url: picture}).then(() => {
        });
    };

    displayFloatingActionButton() {
        if (Platform.OS === 'android') {
            return (
                <TouchableOpacity
                    style={styles.share_touchable_floatingactionbutton}
                    onPress={() => this.shareProduct()}>
                    <Image
                        style={styles.share_image}
                        source={require('../images/ic_share.ios.png')}/>
                </TouchableOpacity>
            )
        }
    }

    componentDidMount() {
        // @ts-ignore
        const product = this.props.products.find(el => el.id === this.props.route.params.product);
        if (product) {
            this.setState({
                product
            });
        }
    }

    componentDidUpdate(prevProps: Readonly<ProductDetailsProps>, prevState: Readonly<ProductItemState>, snapshot?: any) {
        if (prevState.product !== this.state.product) {
            this.props.navigation.setOptions({
                headerRight: () =>
                    (<TouchableOpacity
                        style={styles.share_touchable_headerrightbutton}
                        onPress={this.shareProduct}>
                        <Image
                            style={styles.share_image}
                            source={Platform.OS === "ios" ? require('../images/ic_share.ios.png') : require('../images/ic_share.android.png')}/>
                    </TouchableOpacity>)
            });
        }
    }

    render() {
        if (!this.state.product) {
            return (<View style={styles.main_container}>
                <Text>Produit non existant</Text>
            </View>)
        }
        const {name, description, stock, price, picture, id} = this.state.product;
        return (<View style={{flex: 1}}>
                { id === -1 ? (<ActivityIndicator size={"large"} />) : (<View style={styles.main_container}>
                    <Image
                        style={styles.image}
                        source={{uri: picture}}
                    />
                    <View style={styles.content_container}>
                        <View style={styles.header_container}>
                            <Text style={styles.name_text}>{name}</Text>
                            <Text style={styles.price_text}>{price ? price.toFixed(2) : 0}</Text>
                        </View>
                        <View style={styles.description_container}>
                            <Text style={styles.description_text} numberOfLines={6}>{description}</Text>
                            {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
                        </View>
                        <View style={styles.stock_container}>
                            <Text style={styles.stock_text}>{stock} éléments en stock</Text>
                        </View>
                    </View>
                    {this.displayFloatingActionButton()}
                </View>)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    image: {
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'gray'
    },
    content_container: {
        flex: 1,
        margin: 5
    },
    header_container: {
        flex: 3,
        flexDirection: 'row'
    },
    name_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    price_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666'
    },
    description_container: {
        flex: 7
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666'
    },
    stock_container: {
        flex: 1
    },
    stock_text: {
        textAlign: 'right',
        fontSize: 14
    },
    share_touchable_floatingactionbutton: {
        position: 'absolute',
        width: 60,
        height: 60,
        right: 30,
        bottom: 30,
        borderRadius: 30,
        backgroundColor: '#e91e63',
        justifyContent: 'center',
        alignItems: 'center'
    },
    share_image: {
        width: 30,
        height: 30
    },
    share_touchable_headerrightbutton: {
        marginRight: 8
    }
});

const mapStateToProps = (state: any) => {
    return {
        products: state.products.products
    }
};

export default connect(mapStateToProps)(ProductDetails)
