import React, {FunctionComponent, useEffect, useState} from 'react';
import {ActivityIndicator, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ProductsList, {ProductsListProps} from "./ProductsList";
import {Product} from "./ProductItem";
import SearchBar from "./SearchBar";
import { Ionicons, Entypo } from '@expo/vector-icons';
import {useDispatch, useSelector} from 'react-redux';
import {fetchProducts} from "../actions/products";
import i18n from "i18n-js";

const ProductsPage: FunctionComponent<{navigation: any, dispatch: any}> = (props) => {

    const [filteredProducts, setFilteredProducts] = useState<Array<Product>>([]);
    const [filteringText, setFilteringText] = useState('');
    const [resetSearchBar, setResetSearchBar] = useState(false);
    const dispatch = useDispatch();
    const products = useSelector((state: {products: ProductsListProps}) => state.products.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, []);

    useEffect(() => {
        props.navigation.setOptions({
            headerRight: () =>
                (<TouchableOpacity
                    style={styles.camera_touchable_headerrightbutton}
                    onPress={() => props.navigation.navigate("Caméra")}>
                    <Ionicons name="md-camera" size={32} color="black" />
                </TouchableOpacity>)
        });
    }, []);

    useEffect(() => {
        props.navigation.setOptions({
            headerTitle: products && products.length ? `${i18n.t('productsList')} (${products.length})` : i18n.t('productsList')
        });
        setFilteredProducts(products);
    }, [products]);

    const search = (value: string) => {
        setFilteredProducts(products.filter((el: Product) => el.name.toLowerCase().includes(value.toLowerCase())));
        setFilteringText(value.toLowerCase());
        setResetSearchBar(false);
    };

    const resetFilter = () => {
        setFilteredProducts(products);
        setFilteringText('');
        setResetSearchBar(true);
    };

    return (
        <View style={styles.container}>
            <SearchBar reset={resetSearchBar} onSearch={search} />
            {filteringText ? (<View style={{flexDirection: 'row'}}><Text>{i18n.t('welcome')} : {filteringText}  </Text><Entypo onPress={resetFilter} name="circle-with-cross" size={16} color="black" /></View>) : null}
            {
                products.length === 0 ?
                    <ActivityIndicator style={styles.loader} size="large" /> :
                    <ProductsList navigation={props.navigation} products={filteredProducts} />
            }
        </View>
    );
}

export default ProductsPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  loader: {
      flex: 1
  },
    search_image: {
        width: 30,
        height: 30
    },
    camera_touchable_headerrightbutton: {
        marginRight: 8
    }
});
