import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import Store from './store/configureStore';
import Navigation from "./navigation/navigation";
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import {Text} from "react-native";
// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
    'en-US': {
        filter: 'Filter',
        'semi-column': ':',
        productsList: 'Products list',
        productDetails: 'Product details'
    },
    'fr-FR': {
        filter: 'Filtre',
        'semi-column': ' :',
        productsList: 'Liste de produits',
        productDetails: 'Détail du produit'
    },
};

// Set the locale once at the beginning of your app.
i18n.locale = Localization.locale;

export default class App extends React.Component {
    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> {
        return (
            <Provider store={Store}>
                <Navigation/>
            </Provider>
        );
    }
}