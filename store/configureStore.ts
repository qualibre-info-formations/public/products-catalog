import {applyMiddleware, combineReducers, createStore} from 'redux';
import storeProducts from './reducers/productsReducer';
import thunk from 'redux-thunk';
import storeSearchValue from "./reducers/searchReducer";

const rootReducer = combineReducers({products: storeProducts, search: storeSearchValue});

export default createStore(rootReducer, applyMiddleware(thunk));