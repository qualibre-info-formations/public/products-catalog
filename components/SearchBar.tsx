// components/ProductItem.js

import React from 'react';
import {StyleSheet, View, TextInput, Button} from 'react-native';

export interface SearchBarProps {
    onSearch: Function
    reset: boolean
}

export default class SearchBar extends React.Component<SearchBarProps> {
    state = {
        searchContent: ""
    }

    onSearch = () => {
        this.props.onSearch(this.state.searchContent);
    };

    onChangedText = (text: string) => {
        this.setState({ searchContent: text });
    }

    componentDidUpdate(prevProps: Readonly<SearchBarProps>, prevState: Readonly<{}>, snapshot?: any) {
        if (prevProps.reset !== this.props.reset && this.props.reset) {
            this.setState({searchContent: ''});
        }
    }

    render() {
        return (
            <View style={styles.main_container}>
                <TextInput style={styles.text} onChangeText={this.onChangedText} onSubmitEditing={this.onSearch} value={this.state.searchContent} />
                <View style={styles.button}>
                    <Button title="Rechercher" onPress={this.onSearch} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        justifyContent: 'center',
        height: 50,
        flexDirection: 'row',
        margin: 5
    },
    text: {
        flex: 1,
        height: 50,
        borderWidth: 1
    },
    button: {
        height: 50,
        width: 110
    }
});
