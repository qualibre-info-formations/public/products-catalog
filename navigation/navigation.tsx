// navigation/navigation.js

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image, Platform, StyleSheet, TouchableOpacity} from 'react-native';
import ProductsPage from '../components/ProductsPage';
import ProductDetails from "../components/ProductDetails";
import CameraComponent from "../components/CameraComponent";
import Favorites from "../components/Favorites";
import {NavigationContainer} from "@react-navigation/native";
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import PictureView from "../components/PictureView";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import i18n from "i18n-js";


const ProductsStack = createStackNavigator();

function ProductsStackScreen() {
    return (
        <ProductsStack.Navigator>
            <ProductsStack.Screen name="productsList" options={{title: i18n.t('productsList')}} component={ProductsPage}/>
            <ProductsStack.Screen initialParams={{
                product: -1
            }}
                                  name="productDetails" options={{title: i18n.t('productDetails')}}  component={ProductDetails}/>
            <ProductsStack.Screen name="Caméra" component={CameraComponent}/>
            <ProductsStack.Screen name="Image" component={PictureView}/>
        </ProductsStack.Navigator>
    );
}

const FavoritesStack = createStackNavigator();

function FavoritesStackScreen() {
    return (
        <FavoritesStack.Navigator>
            <FavoritesStack.Screen name="Favoris" component={Favorites}/>
        </FavoritesStack.Navigator>
    );
}

const Tab = Platform.OS === "android" ? createMaterialTopTabNavigator() : createBottomTabNavigator();

export default function TabsNavigator() {
    return (<NavigationContainer>
        {/* @ts-ignore */}
        <Tab.Navigator screenOptions={{
            tabBarShowLabel: false,
            headerShown: false,
            tabBarActiveTintColor: "red"
        }}>
            {/* @ts-ignore */}
            <Tab.Screen name="Produits" options={{
                tabBarIcon: ({ focused, color }: { focused: boolean, color: string }) => <MaterialCommunityIcons name="magnify" size={24} color={color} />
            }} component={ProductsStackScreen}/>
            {/* @ts-ignore */}
            <Tab.Screen name="FavorisTab" options={{
                tabBarIcon: ({ focused, color }: { focused: boolean, color: string }) => (<MaterialIcons name="favorite" size={24} color={color} />)
            }} component={FavoritesStackScreen}/>
        </Tab.Navigator></NavigationContainer>);
}
