// Store/Reducers/productsReducer.js

import {ProductsListProps} from "../../components/ProductsList";
import {AnyAction} from "redux";

const initialState: ProductsListProps = { products: [] };

export default function storeProducts(state = initialState, action: AnyAction) {
    switch (action.type) {
        case 'STORE_PRODUCTS':
            return {...state, products: action.products};
        default:
            return state;
    }
}